<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books;
use Validator;

class BooksController extends Controller
{
    public function add(Request $request){

    	$validator =  Validator::make($request->all(), [
	        'book_name' => 'required|max:255',
	        'author' => 'required|max:255',
	        'cover_image' => 'required|max:255',
	    ]);

	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 
        
        try{
            $data = Books::create([
                'book_name' => $request->input('book_name'),
                'author' => $request->input('author'),
                'cover_image' => $request->input('author'), /*this is supposed to be image added string here just for demo*/
            ]);
            return apiResponse('Book added successfully.',200, NULL,0);
        }
        catch(\Exception $e)
        {	
            throw($e);
        }
    }

    public function edit(Request $request){

    	$validator =  Validator::make($request->all(), [
	        'id' => 'required'
	    ]);

	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 

		$book = Books::where('id', $request->input('id'))->first();
        try{
            Books::where('id', $book->id)->update([
                'book_name' => isset($request->book_name) ? $request->book_name : $book->book_name,
                'author' => isset($request->author) ? $request->author : $book->author,
                'cover_image' => isset($request->cover_image) ? $request->cover_image : $book->cover_image,
            ]);
            return apiResponse('Book updated successfully.',200, NULL,0);
        }
        catch(\Exception $e)
        {	
            throw($e);
        }
    }

    public function delete($book_id){

    	try {
            $data = Books::where('id', $book_id)->delete();
            if($data){
                return response()->json(['status' => 1, 'message' => 'Successfully deleted book']);
            }
            return response()->json(['status' => 0, 'message' => 'Error deleting book']);
        } catch (\Exception $e) {
            throw($e);
        }
    }
}
