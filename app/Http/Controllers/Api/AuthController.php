<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    // Registration Code Block //
    public function register(Request $request)
    {	
    	$validator =  Validator::make($request->all(), [
	        'email' => 'required|unique:users|max:255',
	        'firstname' => 'required|max:255',
	        'lastname' => 'required|max:255',
	        'age' => 'required',
	        'mobile' => 'required',
	        'password' => 'required',
	        'city' => 'required',
	        'gender' => 'required',
	    ]);

	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 

        try{
            $data = User::create([
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'mobile' => $request->input('mobile'),
                'password' => bcrypt($request->input('password')),
                'age' => $request->input('age'),
                'gender' =>  $request->input('gender'),
                'city' =>  $request->input('city'),
            ]);
            return apiResponse(trans('auth.registered'),200, NULL,0);
        }
        catch(\Exception $e)
        {	
            throw($e);
        }
        
    }


    public function login(Request $request){

    	$validator =  Validator::make($request->all(), [
	        'email' => 'required',
	        'password' => 'required',
	    ]);

	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 
        
        try {
                $credentials = $request->only('email', 'password');
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return apiResponse(trans('auth.failed'),401, NULL,-1); // Failure Message
                }
                // all good so return the token
                $user = JWTAuth::user();
                $user->token = $token;
                return apiResponse(trans('auth.login_success'),200, $user,1); // Success Message
            
            
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            throw($e);
        }
    }
}
