<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RentedBooks;
use App\Models\User;
use Validator;

class RentedBooksController extends Controller
{
    public function rent(Request $request){
    	$validator =  Validator::make($request->all(), [
	        'book_id' => 'required',
	    ]);
    	$user_id = getUserid();
	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 

		try{
			$data = RentedBooks::create([
	            'book_id' => $request->input('book_id'),
	            'user_id' => $user_id
	        ]);
	        return apiResponse('Book rented successfully.',200, NULL,1);
	        }
        catch(\Exception $e)
        {	
            throw($e);
        }
	}

	public function return(Request $request){
    	$validator =  Validator::make($request->all(), [
	        'id' => 'required',
	    ]);
    	$user_id = getUserid();
	    if ($validator->fails())
		{
		    return apiResponse(trans('Validation error'),422,  $validator->messages()->all(),0);
		} 

		try{
			$data = RentedBooks::where('id', $request->id)->update([
                'status' => 2 
            ]);
	        return apiResponse('Book returned successfully.',200, NULL,1);
	        }
        catch(\Exception $e)
        {	
            throw($e);
        }
	}

	public function rentedList(Request $request){

		try{
			$data = RentedBooks::where('status', 1)->with('user', 'book')->orderBy('user_id', 'desc')->get();
	        return apiResponse('Rented Books List.',200, $data,1);
	        }
        catch(\Exception $e)
        {	
            throw($e);
        }
	}
}
