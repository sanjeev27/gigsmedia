<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function show(Request $request){

    	try{
            $user_id = getUserid();
            $data = User::where('id', $user_id)->first();
            return apiResponse('Successfully fetched user profile.',200, $data,1); // Success Message
        }
        catch(\Exception $e)
        {
            throw($e);
        }
    }
}
