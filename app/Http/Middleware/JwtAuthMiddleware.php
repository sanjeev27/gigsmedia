<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Http\Middleware\Check;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class JwtAuthMiddleware extends Check
{
    public function handle($request, Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken()) {            
            return apiResponse(trans('auth.token_absent_error'),400 ,null,-1);
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return apiResponse(trans('auth.token_expired_error'),401, null,-1);
        } catch (JWTException $e) {
            return apiResponse(trans('auth.token_invalid_error'),401, null,-1);
        }
        
        return $next($request);
    }
    
}
