<?php

function apiResponse($msg = null,$code = null,$data = null,$status = null)
{
    return response()->json([
        'status' => $status,
        'message' => $msg,
        'data' => $data
    ],$code);
}



function exceptionHandler($e)
{
    return apiResponse(trans('common.exception_message'),500, NULL,-1); // Failure Message
}

function  getUserid(){
    $user = JWTAuth::parseToken()->authenticate();
    return $user->id;
}

function generatePresignedUrl($image_path)
{

    return Storage::disk('gcs')
        ->getAdapter()
        ->getBucket()
        ->object($image_path)
        ->signedUrl(new \DateTime(env('GOOGLE_LINK_EXPIRY','+30 minute')));
}

function getSessionUserId() 
{
    $user = Auth::user();
    return $user->id;
}

function getDeviceDetails(){
    $user = Auth::user();
    $data = ['device_type'=> $user->device_type, 'device_id' => $user->device_id];
    return $data;
}