<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'user',
            'mobile' => 9898989898,
            'email' => 'user1@gigsmedia.com',
            'age' => 28,
            'city' => 'mumbai',
            'password' => bcrypt('12345678'),
        ]);
        DB::table('users')->insert([
            'firstname' => 'user',
            'mobile' => 8989898989,
            'email' => 'user2@gigsmedia.com',
            'age' => 27,
            'city' => 'pune',
            'password' => bcrypt('12345678'),
        ]);
    }
}
