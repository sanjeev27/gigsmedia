<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

     /*
    JWT auth messages
    */

    'token_expired_error'=>'Your token is expired.',
    'token_invalid_error'=>'Your token is invalid.',
    'token_absent_error' => 'Token is required.',
    'token_not_created' => 'Token Not Created.',

    'failed' => 'These credentials do not match our records.',
    'registered' => 'User registered successfully.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_success' => 'User logged in successfully.',

];
