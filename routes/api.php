<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api'],'namespace' => 'Api'], function () {
    Route::post('/auth/register', ['as' => 'auth_register', 'uses' => 'AuthController@register']);
    Route::post('/auth/login', ['as' => 'auth_login', 'uses' => 'AuthController@login']);

});

Route::group(['middleware'=>['api','jwtauth'], 'namespace' => 'Api'], function () {
	Route::get('/user/profile', ['as' => 'user_profile', 'uses' => 'UserController@show']);
	Route::post('/book/add', ['as' => 'add_book', 'uses' => 'BooksController@add']);
	Route::post('/book/edit', ['as' => 'edit_book', 'uses' => 'BooksController@edit']);
	Route::get('/book/delete/{book_id}', ['as' => 'delet_book', 'uses' => 'BooksController@delete']);
	Route::post('/book/rent', ['as' => 'rent_book', 'uses' => 'RentedBooksController@rent']);
	Route::post('/book/return', ['as' => 'rent_book', 'uses' => 'RentedBooksController@return']);
	Route::get('/book/rented_list', ['as' => 'rent_book', 'uses' => 'RentedBooksController@rentedList']);
});

